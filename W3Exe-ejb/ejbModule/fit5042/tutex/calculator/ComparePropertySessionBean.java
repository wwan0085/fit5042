package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.*;

import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.constants.*;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	
	private Set<Property> list;
	
	public ComparePropertySessionBean() {
		list = new HashSet<>();
	}
	
	@Override
	public void addProperty(Property property) {
		list.add(property);
	}
	
	@Override
	public void removeProperty(Property property) {
		for(Property p : list) {
			if(p.getPropertyId() == property.getPropertyId()) {
				list.remove(p);
				break;
			}
		}
	}
	
	@Override
	public int bestPerRoom() {
		Integer bestID = 0;
		int numberOfRoom;
		double price;
		double bestPerRoom = 10000000.00;
		for(Property p: list)
		{
			numberOfRoom = p.getNumberOfBedrooms();
			price = p.getPrice();
			if(price/numberOfRoom < bestPerRoom) {
				bestPerRoom = price/numberOfRoom;
				bestID = p.getPropertyId();
			}
			
		}
		return bestID;
	}

}
